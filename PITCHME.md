# Archibien website & apps development 

@snap[north]
![](assets/img/logo.png)
@snapend


---

### Team

- Olivier : assigne les tâches
- Romain/Simon/... : demande des features / remontent des bugs
- Samuel : lead développeur
- Nicolas : développeur
- Et plus encore 😀 ?

---

### Different projects
- https://archibien.com : site vitrine (site statique commercial)
- https://app.archibien.com : application Archibien (plateforme utilisateurs)
- https://blog.archibien.com : blog Archibien

- https://dev.archibien.com : Archibien developer/technical guide

---
### https://archibien.com
- site vitrine (site statique commercial)
- Actuellement en WordPress
- En cours de migration -> Vue.js (Nuxt.js) +  CSS (Sass)
  - meilleur SEO
  - rapidité
  - flexibilité
  - ajout pages rapides
  - "nouvelle" charte graphique et refonte design
- Repo : `archibien_site`

---
### https://app.archibien.com

- application Archibien (plateforme utilisateurs)
  - *Backend* : Django(Python) (REST API + serveur pages)
  - *Frontend* : Django(Python) + Vue.js(partiellement, mini-SPA)
  - *CSS* : Bootstrap + SASS + ant-design-vue
- Système de déploiement en cours de migration
  - Ajout fonctionnalités plus rapide
  - Robustesse / rigueur
  - Focus sur le produit et non sur le deploiement !
  - Préparer la collaboration
  - 0 downtime !
- Repo : `archibien_app`

---
### https://app.archibien.com

- blog Archibien
- Actuellement en WordPress
- Repo : `archibien_blog` (juste pour l'issue tracker)

---
### Infrastructure

![](assets/img/Architecture_serveurs.png)

---
### Collaborative Development `Slack` and `GitLab`

@snap[north]
![](assets/img/slack_archibien.png)
@snapend


---
### Collaborative Development `Slack`
- Différentes chaînes :
 - dev-app : discuter du dev de l'app
 - dev-site : discuter du dev du site
 - live / preprod-live : notifications de la plateforme via un bot (nouveau projet, nouvelle inscription ...)
 - prod : monitoring avec Sentry de l'app en PROD (bugs, warnings ...)
 - preprod : monitoring avec Sentry de l'app en PREPROD (bugs, warnings ...)

---?image=assets/img/stack/gitlab.png&size=auto 8%&position=top

### Collaborative Development `GitLab (Repos + Issue tracker)`

Utilisation GitLab + GitLab CI au maximum

> GitLab est équivalent à GitHub et possède beaucoup d'outils de CI/CD et d'intégrations avec d'autres apps. GitLab.com (FREE) permet d'avoir des repos privés illimités.

- **All repositories** (Sauf sites WordPress -> la config pourrait d'ailleurs peut-être poussée dans un repo ?)
- **All developers**
---?image=assets/img/stack/gitlab.png&size=auto 8%&position=top

### Collaborative Development `GitLab (Repos + Issue tracker)`

- **Issue tracker** (Bugs, Enhancements, ...)
  - L'utiliser au maximum
  - Pense-bête
  - Toujours mettre :
    - un assigné
    - un/des label(s) (classer et prioriser)
    - un titre clair assez cours
    - une description précise (texte, capture d'écrans -> utiliser le Markdown !)

---?image=assets/img/stack/gitlab.png&size=auto 8%&position=top

### Collaborative Development `GitLab (MR)`

- **Merge request (MR)** Permet de travailler par feature/module/page et de faire des *codes reviews*
 - Toujours travailler sur une branche différente de *develop* et *master* !
 - Crééer sa branche en locale, convention de nommage :
   - feature-*home-page*
   - fix-*bug-responsive*
   - [...]
   - Avant que la feature soit terminée (peut-être fait même 1h après le début du dev !) crééer une merge request via GitLab vers develop avec `WIP` en préfixe du titre -> cela permet au reviewer de déjà suivre le code et de faire des remarques très tôt
 - L'assigner au lead dev

---

### Collaborative Development `GitLab (MR)`

- **Merge request (MR)**
 - Features :
   - Système de commentaires/discussions
   - Visualisation de l'état des pipelines (tests, code quality, linting)
   - Liens vers le site ou cette branche est déployée (review app)
 - Le lead développeur s'occupe d'accepter la merge request ou non :
   - Si qualité code jugée suffisante
   - Tests OK
   - Linting OK
   - Plus de discussions/commentaires.

**WORKFLOW**
1. un dev fait toutes les issues correspondant à la feature
2. il crée une MR
3. le lead dev valide la merge request et l'intègre dans develop si tout est OK
4. sinon il fait des commits jusqu'à que le lead dev valide la MR

---

### Continuous Integration / Continous Delivery `CI/CD`

- Effectué avec `GitLab CI`
- Chaque push déclenche l'intégration continue si activée sur le projet GitLab
- Le fichier `.gitlab-ci.yml` explique les tâches (jobs) à réaliser
- Des *runners* tournent (24/24) sur des serveurs et executent ces tâches.
- Ces tâches sont organisées en pipelines (avec un ordre d'execution)
![GitLab pipelines](assets/img/pipelines.png)
- Automatique ou manuelles, dépendantes ou non

---
### Continuous Integration / Continous Delivery `CI/CD`

- Buts :
  - Lancer les tests unitaires
  - Calculer la couverture de tests (%)
  - Vérifier la syntaxe du code (linting)
  - Analyser la qualité du code/les failles/la sécurité
  - Build
  - Déployer le code sur des serveurs (production, staging)
  - Préparer le café (presque !). *Bref on peut faire tout ce qu'on souhaite*.

---

### Developer documentation `(WIP)`

- Repository `archibien_dev`
- Stack : Django + Vue.js + Storybook + ...
- Goal is to document :
  - Getting started for a developer at Archibien (understand)
  - Explain tools & process (collaborate)
  - Style guide (CSS libraries, colors, fonts, notes)
  - List of Vue.js components developed in-house (playground)
  - Different projects (app algorithms, workflows, models ...)
  - REST API
- Will be available at https://dev.archibien.com
- Automatically built with GitLab-CI
- Collaborative !

---

### Infrastructure (Developer-side)

![](assets/img/Architecture_serveurs_dev.png)
